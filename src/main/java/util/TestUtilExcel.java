package util;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class TestUtilExcel {
    public static Object[][] getData() throws Exception {
        String filepath = "/home/beehyv/Documents/Freecrm_testdata.xlsx";
        File f = new File(filepath);
        FileInputStream fis = new FileInputStream(f);
        XSSFWorkbook book = new XSSFWorkbook(fis);
        book.close();
        XSSFSheet sheet = book.getSheetAt(0);

        int rowcount = sheet.getLastRowNum();
        int colcount = sheet.getRow(0).getLastCellNum();

        Object[][] obj = new Object[rowcount][colcount];
        for(int i=0;i<rowcount;i++){

            for(int j=0;j<colcount;j++){
                obj[i][j] = sheet.getRow(i+1).getCell(j).toString();
            }
        }
        return obj;

    }

}
