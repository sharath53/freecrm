package pages;

import base.TestBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends TestBase {

    //pagefactory
    @FindBy(xpath = "//a[@href='https://ui.freecrm.com']")
    WebElement login_screen;
    @FindBy(xpath = "//input[@name='email']")
    WebElement email_field;
    @FindBy(xpath = "//input[@name='password']")
    WebElement pwd_field;
    @FindBy(xpath = "//div[@class='ui fluid large blue submit button']")
    WebElement login_btn;
    @FindBy(xpath = "//a[@href='https://api.cogmento.com/register']")
    WebElement signup_screen;

    //initializing the page objects
    public LoginPage(){
        PageFactory.initElements(driver, this);
    }

    //actions
    public String verifyLoginPageTitle(){
        return driver.getTitle();
    }

    public HomePage login(String email_id, String pass) throws InterruptedException {
     login_screen.click();
     Thread.sleep(2000);
     email_field.sendKeys(email_id);
     pwd_field.sendKeys(pass);
     login_btn.click();

     return new HomePage();
    }

}
