package pages;

import base.TestBase;
import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.Select;

public class CompaniesPage extends TestBase {
    Actions a = new Actions(driver);
    //PageFactory
    @FindBy(xpath = "//a[@href='/companies/new']")
    WebElement new_btn;
    @FindBy(xpath = "//div[@class='ui right corner labeled input']//input[@name='name']")
    WebElement name_field;
    @FindBy(xpath = "//button[@class='ui small fluid positive toggle button']")
    WebElement access_btn;
    @FindBy(xpath = "//input[@name='address']")
    WebElement addr_field;
    @FindBy(xpath = "//input[@name='city']")
    WebElement city_filed;
    @FindBy(xpath = "//input[@name='state']")
    WebElement state_field;
    @FindBy(xpath = "//input[@name='zip']")
    WebElement zip_field;

    @FindBy(xpath = "//div[@name='country']")
    WebElement country_dropdown;

    @FindBy(xpath = "//div[@class='visible menu transition']/.//span[contains(text(),'"+"India"+"')]")
    List<WebElement> country_specific_field = driver.findElements(By.xpath("//div[@class='visible menu transition']/.//span[contains(text(),'India')]"));

    @FindBy(xpath = "//textarea[@name='description']")
    WebElement desc_field;
    @FindBy(xpath = "//input[@name='industry']")
    WebElement industry_field;
    @FindBy(xpath = "//input[@name='num_employees']")
    WebElement no_of_emp_field;
    @FindBy(xpath = "//input[@name='symbol']")
    WebElement stock_sym_field;

    @FindBy(xpath = "//button[@class='ui linkedin button']")
    WebElement save_btn;

    //Actions
    public CompaniesPage(){
        PageFactory.initElements(driver, this);
    }
    public void createCompany() throws InterruptedException {

        //loop for creating objects with diff names
//        for(int j=2010;j<=2015;j++){
//            new_btn.click();
//            Thread.sleep(2000);
//            name_field.sendKeys("SysInfo "+j+"");
//            save_btn.click();
//
//            Thread.sleep(2000);
//            driver.findElement(By.xpath("//span[contains(text(),'Companies')]")).click();
//            driver.navigate().refresh();
//            Thread.sleep(2000);
//
//        }

        new_btn.click();
        Thread.sleep(2000);
        name_field.sendKeys("SysInfo");

          access_btn.click();
          addr_field.sendKeys("Kothapet");
          city_filed.sendKeys("Hyderabad");
          state_field.sendKeys("Telangana/India");
          zip_field.sendKeys("500045");

          country_dropdown.click();
          Thread.sleep(1000);

          for(WebElement we:country_specific_field){
              if(we.getText().equals("India")){
                  we.click();
              }
          }


          desc_field.sendKeys("FreeCRM Create Company Description field");
          industry_field.sendKeys("Industry");
          no_of_emp_field.sendKeys("50");
          stock_sym_field.sendKeys("Stock symbol");

        save_btn.click();

        Thread.sleep(2000);
        driver.findElement(By.xpath("//span[contains(text(),'Companies')]")).click();
        driver.navigate().refresh();
        Thread.sleep(2000);

    }

    public void turnPagesPagination() throws InterruptedException {

        driver.navigate().refresh();
        Thread.sleep(2000);
        List<WebElement> pages_bars = driver.findElements(By.xpath("//div[contains(@class,'ui right floated pagination menu custom-pagination')]/.//a"));

        for(WebElement pages: pages_bars){
            pages.click();
            Thread.sleep(3000);
        }
    }
}
