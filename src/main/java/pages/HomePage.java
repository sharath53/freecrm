package pages;

import base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends TestBase {

    //pagefactory
    @FindBy(xpath = "//span[@class='user-display']")
    WebElement user_info;
    @FindBy(xpath = "//a[@href='/contacts']")
    WebElement contacts_link;
    @FindBy(xpath = "//a[@href='/companies']")
    WebElement companies_link;
    @FindBy(xpath = "//a[@href='/tasks']")
    WebElement tasks_link;
    @FindBy(xpath = "//a[@href='/settings/billing/']")
    WebElement trail_period;

    public HomePage(){
        PageFactory.initElements(driver,this);
    }

    public String verifyHomePageTitle(){
        return driver.getTitle();
    }
    public String verifyUserName(){
        return user_info.getText();
    }
    public ContactsPage clickOnContactsLink(){
        contacts_link.click();
        return new ContactsPage();
    }
    public CompaniesPage clickOnCompaniesLink(){
        companies_link.click();
        return new CompaniesPage();
    }
    public void clickOnTasksLink(){
        tasks_link.click();
    }
    public void clickOnTrailPeriod(){
        trail_period.click();
    }
}
