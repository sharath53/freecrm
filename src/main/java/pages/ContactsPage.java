package pages;

import base.TestBase;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

public class ContactsPage extends TestBase {

    JavascriptExecutor js = (JavascriptExecutor) driver;
    Actions a = new Actions(driver);

    //PageFactory
    @FindBy(xpath = "//a[@href='/contacts/new']")
    WebElement new_contact_btn;
    @FindBy(xpath = "//input[@name='first_name']")
    WebElement first_name;
    @FindBy(xpath = "//input[@name='last_name']")
    WebElement last_name;
    @FindBy(xpath = "//input[@placeholder='Email address']")
    WebElement email_id;
    @FindBy(xpath="//input[@name='do_not_call']")
    WebElement do_not_call_btn;
    @FindBy(xpath = "//input[@name='do_not_text']")
    WebElement do_not_text_btn;
    @FindBy(xpath = "//input[@placeholder='Day']")
    WebElement day_field;
    @FindBy(xpath = "//div[@name='month']")
    WebElement month_fied;
    @FindBy(xpath = "//div[@class='visible menu transition']/.//span[contains(text(),'"+"April"+"')]")
    WebElement month_specific_field;
    @FindBy(xpath = "//input[@name='year']")
    WebElement year_field;
    @FindBy(xpath = "//input[@name='image']")
    WebElement image_filed;
    @FindBy(xpath = "//button[@class='ui linkedin button']")
    WebElement save_btn;
    @FindBy(xpath = "//a[@href='/contacts/3ca8761c-3b87-40f3-8d5d-02cb6bd6c946']")
    WebElement show_profile_btn;
    @FindBy(xpath = "//a[@href='/contacts/edit/3ca8761c-3b87-40f3-8d5d-02cb6bd6c946']")
    WebElement edit_profile_btn;
    @FindBy(xpath = "//tbody/tr[1]/td[6]/div/button")
    WebElement delete_profile_btn;
    @FindBy(xpath = "//button[@class='ui button']")
    WebElement confirm_del_btn;
    @FindBy(xpath = "//tbody/tr[7]/td/div/input[@name='id']")
    WebElement del_checkbox;
    @FindBy(xpath = "//div[@name='action']")
    WebElement del_dropdown;
    @FindBy(xpath = "//i[@class='checkmark icon']")
    WebElement group_level_del_checkbox;
    @FindBy(xpath = "//button[@class='ui primary button']")
    WebElement confirm_del_checkbox;

    //actions
    public  ContactsPage(){
        PageFactory.initElements(driver, this);
    }
    public void createContact() throws InterruptedException {
        new_contact_btn.click();
        Thread.sleep(2000);
        first_name.sendKeys("Bharath");
        last_name.sendKeys("Tagore");
        email_id.sendKeys("aanand@gmail.com");

        js.executeScript("window.scrollBy(0,1000)");

        a.moveToElement(do_not_call_btn).click().build().perform();

        a.moveToElement(do_not_text_btn).click().build().perform();

        day_field.sendKeys("15");

        month_fied.click();
        Thread.sleep(2000);

        month_specific_field.click();

        year_field.sendKeys("2000");
        image_filed.sendKeys("/home/beehyv/Pictures/Wallpapers/1.jpeg");
        save_btn.click();
    }

    public void showProfile(){
        show_profile_btn.click();
    }
    public void editProfile(){
        edit_profile_btn.click();
    }
    public void deleteProfile() throws InterruptedException {
        driver.navigate().refresh();
        delete_profile_btn.click();
        Thread.sleep(1000);
        confirm_del_btn.click();
    }
    public void deleteProfileThrCheckBox() throws InterruptedException {
        driver.navigate().refresh();
        js.executeScript("window.scrollBy(0,500)");

        a.moveToElement(del_checkbox).click().build().perform();

        del_dropdown.click();

        //group_level_del_checkbox.click();
        Thread.sleep(1000);
        confirm_del_checkbox.click();
    }
}
