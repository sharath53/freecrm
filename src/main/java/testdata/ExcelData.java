package testdata;

import org.testng.annotations.DataProvider;
import util.TestUtilExcel;


public class ExcelData {

    @DataProvider(name = "getTestData")
    public Object[] getTestData() throws Exception {
        return TestUtilExcel.getData();
    }

}
