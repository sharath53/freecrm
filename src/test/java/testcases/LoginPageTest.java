package testcases;

import base.TestBase;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.LoginPage;

public class LoginPageTest extends TestBase {

    LoginPage loginPage;

    public LoginPageTest(){
        super();
    }

    @BeforeMethod
    public void setUp(){
        initialization();
        loginPage = new LoginPage();
    }

    @Test(priority = 1)
    public void validateLoginPageTitle(){
        String a_title = loginPage.verifyLoginPageTitle();
        System.out.println("Login page title: "+a_title);
        Assert.assertEquals(a_title,"Free CRM #1 cloud software for any business large or small","Login Page title not matched");
    }

    @Test(priority = 2)
    public void login() throws InterruptedException {
        loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
    }

//    @AfterMethod
//    public void exitFrmWindow(){
//        driver.quit();
//    }

}
