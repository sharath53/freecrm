package testcases;

import base.TestBase;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.ContactsPage;
import pages.HomePage;
import pages.LoginPage;

public class ContactsPageTest extends TestBase {

    LoginPage loginPage;
    HomePage homePage;
    ContactsPage contactsPage;

    public ContactsPageTest(){
        super();
    }

    @BeforeMethod
    public void setUpContactsPageTest() throws InterruptedException {
        initialization();
        loginPage = new LoginPage();
        homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
        contactsPage = homePage.clickOnContactsLink();
    }
//    @Test(priority = 1)
//    public void verifyCreateContactTest() throws InterruptedException {
//        contactsPage.createContact();
//    }
    @Test(priority = 2)
    public void verifyShowProfileTest(){
        contactsPage.showProfile();
    }
    @Test(priority = 3)
    public void verifyEditProfileTest(){
        contactsPage.editProfile();
    }
    @Test(priority = 4)
    public void verifyDeleteProfileTest() throws InterruptedException {
        contactsPage.deleteProfile();
    }
    @Test(priority = 5)
    public void deleteProfileThrCheckBoxTest() throws InterruptedException {
        contactsPage.deleteProfileThrCheckBox();
    }
}
