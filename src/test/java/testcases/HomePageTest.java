package testcases;

import base.TestBase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

public class HomePageTest extends TestBase {

    LoginPage loginPage;
    HomePage homePage;
    public HomePageTest(){
        super();
    }

    @BeforeMethod
    public void setUp() throws InterruptedException {
        initialization();
         loginPage = new LoginPage();
         homePage = new HomePage();
         homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
    }

    @Test(priority = 1)
    public void validateHomePageTitle(){
        String home_title = homePage.verifyHomePageTitle();
        System.out.println("Home Page title is: "+home_title);
        Assert.assertEquals(home_title,"Cogmento CRM","Home page title not matched");
    }

    @Test(priority = 2)
    public void verifyContactsPage(){
        homePage.clickOnContactsLink();
    }

    @Test(priority = 3)
    public void verifyCompaniesPage(){
        homePage.clickOnCompaniesLink();
    }
    @Test(priority = 4)
    public void verifyTasksPage(){
        homePage.clickOnTasksLink();
    }
    @Test(priority = 5)
    public void validateUserName(){
        String userinfo = homePage.verifyUserName();
        System.out.println("username is: "+userinfo);
        Assert.assertEquals(userinfo,"Sharath G","username not matched");
    }
    @Test(priority = 6)
    public void validateAccountPage(){
        homePage.clickOnTrailPeriod();
    }
}
