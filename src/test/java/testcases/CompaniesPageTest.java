package testcases;

import base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CompaniesPage;
import pages.HomePage;
import pages.LoginPage;
import testdata.ExcelData;
import util.TestUtilExcel;

import java.util.ArrayList;
import java.util.Iterator;

public class CompaniesPageTest extends TestBase {

    LoginPage loginPage;
    HomePage homePage;
    CompaniesPage companiesPage;

    public CompaniesPageTest(){
        super();
    }

    @BeforeMethod
    public void setUpCompaniesPageTest() throws InterruptedException {
        initialization();
        loginPage = new LoginPage();
        homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
        companiesPage = homePage.clickOnCompaniesLink();
    }

    //toolsqa demo method
    @Test(dataProvider = "getTestData",dataProviderClass = ExcelData.class)
    public void loadToolsQAPageTest(String firstname, String lastname, String country){
        driver.navigate().to("https://demoqa.com/html-contact-form/");//toolsqa demo page
        driver.findElement(By.xpath("//input[@class='firstname']")).sendKeys(firstname);
        driver.findElement(By.id("lname")).sendKeys(lastname);
        driver.findElement(By.xpath("//input[@name='country']")).sendKeys(country);
    }

//will repeat the test 5 times
//    @Test(invocationCount = 5)
//    public void sum(){
//        System.out.println("hello");
//    }
//    @Test(priority = 1)
//    public void verifyCreateCompanyTest() throws InterruptedException {
//            companiesPage.createCompany();
//    }
//    @Test(priority = 2)
//    public void verifyPaginationTest() throws InterruptedException {
//        companiesPage.turnPagesPagination();
//    }

//    @AfterMethod
//    public void clearWindow() throws InterruptedException {
//        driver.quit();
//    }
}
